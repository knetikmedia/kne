/**
 * Created by brandon on 1/14/16.
 */

var _ = require('lodash'),
    http = require('http'),
    net = require('net'),
    ioRedis = require('socket.io-redis'),
    cluster = require('cluster');

/*
 KNE Socket Websocket Submodule
 A module that initializes a websocket server built with Socket.IO.
 */
var io = null;
function KneWebsocket() {
    var self = this;
}

KneWebsocket.prototype.init = function(defer) {
    var self = this,
        server = http.createServer(self.httpRequestHandler),
        kneRedis = self.engine.getSubmodule('kne-redis');

    io = require('socket.io')(server);
    io.adapter(ioRedis(kneRedis.getClient()));

    process.on('message', function(message, connection){
        if (message.type !== 'sticky-session:connection') {
            return;
        }

        server.emit('connection', connection);
        connection.push(new Buffer(message.data));
        connection.resume();
    });

    server.listen(0, 'localhost');
    server.once('listening', function() {
        try {
            defer.resolve();
        } catch (e) {
            defer.reject(e);
        }
    });

    server.on('error', function(err) {
        if (!self.engine.hasInitialized) {
            defer.reject(err);
        } else {
            self.log.error(err);
        }
    });

    io.on('connection', function(socket) {
        if (socket.userData) {
            socket.join('user:' + socket.userData.id);
        }
    });
};

KneWebsocket.prototype.getClient = function() {
    if (!io) {
        throw new Error('Websocket client has not yet been initialized.');
    }
    return io;
};

KneWebsocket.prototype.httpRequestHandler = function(req, res) {
    res.writeHead(200, {"Content-Type": "text/html"});
    res.write('ok');
    res.end();
};

KneWebsocket.prototype.addMiddleware = function(middleware) {
    if (!io) {
        throw new Error('Websocket client has not yet been initialized.');
    }
    io.use(middleware);
};

module.exports = KneWebsocket;
module.exports.configs = {
    port: 14000
};

//An init function only called on the master process that does not init an instance of this module.
module.exports.initMaster = function(defer, engine, log) {
    //This function is used to set up a server that passes connections to the same worker each time, so that we retain
    //a sticky session.
    var port = engine.getConfig('kne-websocket.port');
    var server = net.createServer({},
        function(connection) {
            // We received a connection and need to pass it to the appropriate worker. Get the worker for this
            // connection's IP and pass it the connection.
            var realIp = connection.remoteAddress;
            connection.on('data', function(data) {
                connection.removeAllListeners('data');
                connection.pause();

                var s = data.toString('ascii'),
                    sarray = s.split('\r\n');

                //Needed to parse the packet for the X-Forwarded-For header, in case of a load balancer.
                _.forEach(sarray, function(h) {
                    if (h.indexOf('X-Forwarded-For:') > -1) {
                        realIp = h.replace('X-Forwarded-For:', '').trim();
                    }
                });

                realIp = realIp.replace('::ffff:', '');

                var id = getWorkerIndex(realIp, engine.getConfig('kne.workers'));
                var worker = cluster.workers[id];
                if (worker) {
                    worker.send({type: 'sticky-session:connection', data: data}, connection);
                } else {
                    log.error('Worker not found for request. WID: ' + id + ' IP: ' + realIp);
                }
            });
        }
    );

    //Get a worker id based on the connecting IP, this is so we can have a sticky connection.
    function getWorkerIndex(ip, len) {
        var s = 0;
        for (var i = 0, _len = ip.length; i < _len; i++) {
            s += ip.charCodeAt(i);
        }
        var num = (Number(s) % len);
        return Object.keys(cluster.workers)[num];
    }

    server.listen(port);
    server.once('listening', function() {
        try {
            defer.resolve();
        } catch (e) {
            defer.reject(e);
        }
    });

    server.on('error', function(err) {
        if (!engine.hasInitialized) {
            defer.reject(err);
        } else {
            log.error(err);
        }
    });
};
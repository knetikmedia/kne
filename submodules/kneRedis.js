/**
 * Created by brandon on 1/14/16.
 */

var _ = require('lodash'),
    redis = require('redis');

/*
 KNE Redis Submodule
 A module that initializes a connection with a Redis server.
 */
var client = null;
function KneRedis() {
    var self = this;
}

KneRedis.prototype.init = function(defer) {
    var self = this,
        url = self.engine.getConfig('kne-redis.url');

    client = redis.createClient(url);

    client.on('ready', function() {
        defer.resolve();
    });

    client.on('error', function(err) {
        if (!self.engine.hasInitialized) {
            defer.reject(err);
        } else {
            self.log.error(err);
        }
    });
};

KneRedis.prototype.getClient = function() {
    if (!client) {
        throw new Error('Redis client has not yet been initialized.');
    }
    return client;
};

module.exports = KneRedis;
module.exports.configs = {
    url: 'redis://127.0.0.1:6379'
};
/**
 * Created by brandon on 1/8/16.
 */

var jsapi = require('jsapi-js-sdk'),
    Q = require('q');

/*
    KNE JSAPI Submodule
    A module that initializes a JSAPI token for use between workers.
 */
var client = null;
function KneJsapi() {
    var self = this;
}

KneJsapi.prototype.init = function(defer) {
    var self = this;

    jsapi.enableDebugging(self.engine.getConfig('kne-jsapi.enableDebugging'));
    jsapi.setLogger(self.log);
    jsapi.setConfig(self.engine.getConfig('kne-jsapi'));

    jsapi.auth.serverAuth().done(function(token) {
        client = jsapi.client(token);
        client.onTokenExpired = self.onTokenExpired.bind(self);
        defer.resolve();
    }, function(err) {
        defer.reject(err);
    });
};

KneJsapi.prototype.onTokenExpired = function() {
    var self = this,
        defer = Q.defer();

    self.log.warn('JSAPI token invalid/expired, attempting to get a new one.');
    jsapi.auth.serverAuth().done(function(token) {
        self.log.info('Got new JSAPI token.');
        defer.resolve(token);
    }, function(err) {
        self.log.error('Failed to get new JSAPI token:', err);
        defer.reject(err);
    });

    return defer.promise;
};

KneJsapi.prototype.getClient = function() {
    if (!client) {
        throw new Error('JSAPI client has not yet been initialized.');
    }
    return client;
};

KneJsapi.prototype.authenticationMiddleware = function(socket, next) {
    var self = this;

    if (socket.handshake.query.token) {
        var jsapiClient = self.getClient();
        jsapiClient.Services.User.getInfo({accessToken: socket.handshake.query.token}).then(function (userData) {
            userData.token = socket.handshake.query.token;
            socket.userData = userData;
            next();
        }, function (err) {
            if (err.statusCode >= 400 && err.statusCode < 500) {
                next(new Error('Failed to authenticate websocket, invalid token.'));
            } else{
                next(new Error('Internal server error, failed to login.'));
                self.log.error('Failed to authenticate websocket, JSAPI error:', err);
            }
        });
    } else {
        next(new Error('No token was provided.'));
    }
};

module.exports = KneJsapi;
module.exports.configs = {
    url: undefined,
    clientId: undefined,
    clientSecret: undefined,
    grantPath: '/oauth/token',
    'enableDebugging': false
};
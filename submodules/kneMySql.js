/**
 * Created by brandon on 4/18/16.
 */

var _ = require('lodash'),
    mysql = require('mysql'),
    Q = require('q');

/*
 KNE MySQL Submodule
 A module that initializes a MySQL connection and executes various boilerplate code.
 */
var connectionPool = null;
function kneMySql() {
    var self = this;
}

kneMySql.prototype.init = function(defer) {
    var self = this;

    connectionPool = mysql.createPool(_.merge({
        host: self.engine.getConfig('kne-mysql.host'),
        port: self.engine.getConfig('kne-mysql.port'),
        user: self.engine.getConfig('kne-mysql.user'),
        password: self.engine.getConfig('kne-mysql.password'),
        connectionLimit: self.engine.getConfig('kne-mysql.connectionLimit')
    }, self.engine.getConfig('kne-mysql.options')));

    connectionPool.on('error', function(err) {
        console.error('MySQL error:', err.stack ? err.stack : err);
        self.log.error('MySQL error:', err.stack ? err.stack : err);
    });

    //Test the connection pool when starting up.
    self.getConnection().then(function(connection) {
        connection.release();
        defer.resolve();
    }, function(err) {
        defer.reject(err);
    });
};

/*
When getting a connection from the pool be sure to call connection.release() to put it back into the pool.
 */
kneMySql.prototype.getConnection = function() {
    var defer = Q.defer();

    connectionPool.getConnection(function(err, connection) {
       if (err) {
           defer.reject(err);
       } else {
           defer.resolve(connection);
       }
    });

    return defer.promise;
};

kneMySql.prototype.getPool = function() {
    return connectionPool;
};

module.exports = kneMySql;
module.exports.configs = {
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: 'mysql',
    connectionLimit: 2,
    options: {}
};
/**
 * Created by brandon on 1/8/16.
 */

var express = require('express'),
    expressApp = express(),
    bodyParser = require('body-parser'),
    fs = require('fs'),
    _ = require('lodash');

/*
    KNE Express Submodule
    A web server built with express.

    * Express routes must be defined in a modules init function so that the postInit of this module can apply
    the proper error handling.
 */

function KneExpress() {
    var self = this;
}

KneExpress.prototype.init = function(defer) {
    var self = this;
    self.app = expressApp;

    self.app.use(bodyParser.json());

    self.app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type, Accept');
        res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        return next();
    });

    var port = self.engine.getConfig('kne-express.port');
    self.app.listen(port, function () {
        self.log.debug('Express server started on port: ' + port);
        defer.resolve();
    });

    self.app.get('/api/status', function(req, res) {
        res.send({success: true});
    });
};

KneExpress.prototype.postInit = function(defer) {
    var self = this;

    self.loadRoutes();
    self.app.use(self.handleError.bind(this));
    defer.resolve();
};

//Goes through each module and loads any *.route.js files in the routes folder.
KneExpress.prototype.loadRoutes = function() {
    var self = this,
        modulePaths = this.engine.getModulePaths();

    _.forEach(modulePaths, function(path, name) {
        var req = require(path);

        path = require.resolve(path);
        path = path.substring(0, path.length-7);
        if (req.submodules && _.contains(req.submodules, 'kne-express')) {
            try {
                var files = fs.readdirSync(path + '/routes');
                _.forEach(files, function(file) {
                    if (file.endsWith('.route.js')) {
                        self.log.debug('Loading route "' + file + '".');
                        try {
                            var routeReq = require(path + '/routes/' + file);
                            routeReq(self.engine.getModule(name), self.engine, self.app);
                        } catch (e) {
                            self.log.error('Failed to load route "' + file + '" for module "' + name + '":', e);
                            process.exit(1);
                        }
                    }
                });
            } catch (e) {
                if (!(e.code === 'ENOENT' && e.path === path + '/routes')) {
                    self.log.error('Failed to load routes for module "' + name + '":', e);
                    process.exit(1);
                }
            }
        }
    });
};

KneExpress.prototype.handleError = function(err, req, res, next) {
    var trace = err.stack;
    if (!trace) {
        trace = err;
        if (typeof trace === 'object') {
            if (trace.message) {
                trace = trace.message;
            } else {
                try {
                    trace = JSON.stringify(err);
                } catch (e) {}
            }
        }
    }

    if (!err.statusCode) {
        if (typeof err === 'object') {
            err.statusCode = 500;
        } else {
            err = {
                message: err,
                statusCode: 500
            };
        }
    } else if (err.type === 'jsapiError' || (err.message && err.response && err.code)) {
        //If the error object has these, assume it is a JSAPI error.
        if (typeof trace === 'string') {
            trace = 'JSAPI RESPONSE ERROR: ' + trace;
        } else {
            trace = 'JSAPI RESPONSE ERROR: ';
        }

        var addToTrace = function (key) {
            var val = _.get(err, key);
            if (val) {
                trace += '\n\t' + key + ': ' + val;
            }
        };

        addToTrace('code');
        addToTrace('message');
        addToTrace('statusCode');
        addToTrace('response.statusMessage');
        addToTrace('response.rawEncoded');
        addToTrace('url');

        //Set the status code to a 500, we shouldn't be letting JSAPI errors make it this far in production.
        err.statusCode = 500;
    }

    if (err.statusCode >= 300 && err.statusCode < 500) {
        this.log.warn("EXPRESS REQUEST ERROR:", err.stack ? err.stack : err);
    } else {
        this.log.error("EXPRESS REQUEST ERROR:", err.stack ? err.stack : err);
    }

    var errType = '';
    if (err.statusCode >= 300 && err.statusCode < 400) {
        errType = 'Redirection';
    } else if (err.statusCode >= 400 && err.statusCode < 500) {
        errType = 'Client error';
    } else {
        errType = 'Internal server error';
    }

    res.status(err.statusCode);
    if (process.env.NODE_ENV === 'development') {
        var resp = '<h1>' + err.statusCode + ' - ' + errType + '</h1>';
        resp += '\n<xmp style="background-color:#CCCCCC; padding:12px; border: #000 solid 2px;">' + trace + '</xmp>';
        resp += '\n<br><div align="center"><img width="512" src="https://http.cat/' + err.statusCode + '"></div>';
        res.send(resp);
    } else {
        //TODO: May need to implement something for custom error pages.
        var message = err.statusCode + ' - ' + errType;
        if (err.message) {
            message += ": " + err.message;
        }
        res.send(message);
    }

    if (err.statusCode === 500) {
        process.exit(1);
    }
};

module.exports = KneExpress;
module.exports.configs = {
    port: 8000
};
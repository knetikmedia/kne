/**
 * Created by brandon on 2/8/16.
 */

var _ = require('lodash'),
    kue = require('kue'),
    reds = require('reds');

/*
 KNE Queue Submodule
 A module that initializes a job queue in Redis.
 */
var queue = null;
var searchKeys = {};
function KneQueue() {
    var self = this;
}

kue.Job.prototype.searchKeys = function(keys) {
    var self = this;
    if (!searchKeys[this.type]) {
        searchKeys[this.type] = {};
    }

    if (keys) {
        _.forEach(keys, function (key) {
            searchKeys[self.type][key] = 1;
        });
    }

    if( 0 === arguments.length ) return this._searchKeys;
    this._searchKeys = keys || [];
    if( !_.isArray(this._searchKeys) ) {
        this._searchKeys = [ this._searchKeys ];
    }
    return this;
};

KneQueue.prototype.init = function(defer, isMaster) {
    var self = this,
        redisUrl = self.engine.getConfig('kne-queue.redisUrl');

    queue = kue.createQueue({
        redis: redisUrl,
        disableSearch: false
    });

    var search;
    function getSearch() {
        if( search ) return search;
        search = reds.createSearch(queue.client.getKey('search'));
        return search;
    }

    var uiPort = parseInt(self.engine.getConfig('kne-queue.uiPort')),
        removeJobsOnComplete = parseInt(self.engine.getConfig('kne-queue.removeJobsOnComplete'));
    if (isMaster) {
        if (process.env.NODE_ENV === 'development') {
            queue.client.flushdb();
        }

        if (uiPort) {
            kue.app.listen(uiPort, function () {
                defer.resolve();
            });
        } else {
            defer.resolve();
        }

        if (removeJobsOnComplete) {
            queue.on('job complete', function (id) {
                kue.Job.get(id, function (err, job) {
                    if (err) return;
                    job.remove(function (err) {
                        if (err) {
                            self.log.error('Failed to remove completed job:', err);
                        }
                    });
                });
            });
        }
    } else {
        if (!uiPort) {
            var kneExpress = self.engine.getSubmodule('kne-express'),
                clientSecret = self.engine.getConfig('kne-jsapi.clientSecret');

            kneExpress.app.use('/api/queue', function(req, res, next) {
                if (req.method === 'OPTIONS') {
                    next();
                } else {
                    if (req.headers.authorization === clientSecret) {
                        next();
                    } else {
                        next({statusCode: 401, message: 'Invalid authorization.'});
                    }
                }
            });

            queue.totalCard = function(fn) {
                this.client.zcard(this.client.getKey('jobs'), fn);
                return this;
            };


            kneExpress.app.get('/api/queue/job/search', function(req, res, next) {
                var q = getSearch().query(req.query.q);

                q.end = function(fn){
                    var key = this.search.key;
                    var db = this.search.client;
                    var query = this.str;
                    var words = reds.stem(reds.stripStopWords(reds.words(query)));
                    var keys = reds.metaphoneKeys(key, words);
                    var type = this._type;
                    var start = this._start || 0;
                    var stop = this._stop || -1;

                    if (!keys.length) return fn(null, []);

                    var tkey = key + 'tmpkey';
                    db.multi([
                        [type, tkey, keys.length].concat(keys),
                        ['zrevrange', tkey, start, stop],
                        ['zremrangebyrank', tkey, start, stop]
                    ]).exec(function(err, ids) {
                        if (err) {
                            return fn(err);
                        }
                        ids = ids[1];

                        var nids = [],
                            cdone = 0;
                        _.forEach(ids, function(id) {
                            if (nids) {
                                kue.Job.get(id, function (err, job) {
                                    if (err) {
                                        nids = null;
                                        fn(err, null);
                                    } else {
                                        if (nids) {
                                            if (searchKeys[job.type]) {
                                                _.forEach(searchKeys[job.type], function(value, key) {
                                                    if (_.get(job.data, key) == query) {
                                                        nids.push(id);
                                                        return false;
                                                    }
                                                });
                                            } else {
                                                if (job.data.id == query) {
                                                    nids.push(id);
                                                }
                                            }
                                        }
                                    }
                                    cdone++;

                                    if (cdone >= ids.length) {
                                        fn(null, nids);
                                    }
                                });
                            } else {
                                return false;
                            }
                        });
                    });

                    return this;
                }.bind(q);

                q.type('and').end(function( err, ids ) {
                    if(err) {
                        res.json({error: err.message});
                    } else {
                        res.json(ids);
                    }
                });
            });

            kneExpress.app.use('/api/queue', kue.app);
            kneExpress.app.get('/api/queue/jobs/count', function(req, res, next) {
                queue.totalCard(function(err, count) {
                    if (err) {
                        self.log.error('Job Count Error:', err);
                        next({statusCode: 500, message: 'Failed to get count because of an error.'});
                    } else {
                        res.send('' + count);
                    }
                });
            });

            kneExpress.app.put('/api/queue/job/:id/data', function(req, res, next) {
                kue.Job.get(req.params.id, function(err, job) {
                    if (err) {
                        next({statusCode: 400, message: err});
                    } else {
                        job.data = req.body;
                        job.update(function(err) {
                            if (err) {
                                next({statusCode: 500, message: err});
                            } else {
                                res.send({success: true});
                            }
                        });
                    }
                });
            });
            defer.resolve();
        } else {
            defer.resolve();
        }
    }
};

KneQueue.prototype.getQueue = function() {
    if (!queue) {
        throw new Error('Queue client has not yet been initialized.');
    }
    return queue;
};

module.exports = KneQueue;
module.exports.configs = {
    redisUrl: 'redis://127.0.0.1:6379',
    removeJobsOnComplete: false,
    uiPort: null
};
module.exports.initMaster = true;

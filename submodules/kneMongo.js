/**
 * Created by brandon on 1/8/16.
 */

var _ = require('lodash'),
    mongoose = require('mongoose'),
    fs = require('fs');

/*
    KNE Mongo Submodule
    A module that initializes a Mongo connection, loads module models, and executes various other boilerplate code.

    * This module will load any *.model.js files located in any other modules models folder that requires this module.
 */
function KneMongo() {
    var self = this;
}

KneMongo.prototype.init = function(defer) {
    var self = this;

    var url = self.engine.getConfig('kne-mongo.url');
    mongoose.connect(url);
    mongoose.Promise = require('q').Promise;
    
    var db = mongoose.connection;
    db.once('open', function() {
        defer.resolve();
    });

    db.on('error', function(err) {
        if (self.engine.hasInitialized) {
            self.log.error(err);
            process.exit(1);
        } else {
            defer.reject(err);
        }
    });
};

KneMongo.prototype.postInit = function(defer) {
    var self = this;
    self.loadModels();
    defer.resolve();
};

//Goes through each module and loads any .model.js files in the models folder.
KneMongo.prototype.loadModels = function() {
    var self = this,
        modulePaths = this.engine.getModulePaths();

    _.forEach(modulePaths, function(path, name) {
        var req = require(path);

        path = require.resolve(path);
        path = path.substring(0, path.length-7);
        if (req.submodules && _.contains(req.submodules, 'kne-mongo')) {
            try {
                var files = fs.readdirSync(path + '/models');
                _.forEach(files, function(file) {
                    if (file.endsWith('.model.js')) {
                        self.log.debug('Loading model "' + file + '".');
                        try {
                            var modelReq = require(path + '/models/' + file);
                            modelReq(self.engine.getModule(name), self.engine);
                        } catch (e) {
                            self.log.error('Failed to load model "' + file + '" for module "' + name + '":', e);
                            process.exit(1);
                        }
                    }
                });
            } catch (e) {
                if (!(e.code === 'ENOENT' && e.path === path + '/models')) {
                    self.log.error('Failed to load models for module "' + name + '":', e);
                    process.exit(1);
                }
            }
        }
    });
};

KneMongo.prototype.getModel = function(name) {
    try {
        return mongoose.model(name);
    } catch (e) {
        this.log.error('Failed to get model "' + name + '":', e);
        process.exit(1);
        return null;
    }
};

module.exports = KneMongo;
module.exports.configs = {
    url: 'mongodb://127.0.0.1/kne-default'
};

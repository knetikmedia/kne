/**
 * Created by brandon on 2/8/16.
 *
 * A simple engine init test, that inits a test module that requires every submodule.
 */

global.KNE_BASE_PATH = __dirname;

var engine = require('../../app'),
    cluster = require('cluster');

if (cluster.isMaster) {
    console.log("Running KNE test init.");
}

engine.init([
    'test-master-module',
    'test-module'
]).then(function() {
    if (cluster.isMaster) {
        console.log('Test init complete.');
    }
}, function(err) {
    console.error('Test init failure:', err);
});

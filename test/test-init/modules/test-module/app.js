/**
 * Created by brandon on 1/7/16.
 */

var _ = require('lodash'),
    Q = require('q'),
    http = require('http');

function TestService() {
    var self = this;
}

TestService.prototype.init = function(defer) {
    var self = this;

    var test1 = self.engine.getConfig('test-module.test1');
    if (test1 !== 'teststring') {
        throw new Error('Configuration test failed, configuration "test1" is "' + test1 + '" not "teststring".');
    }

    var test2 = self.engine.getConfig('test-module.test2');
    if (test2 !== 'teststringoverride') {
        throw new Error('Configuration test failed, configuration "test2" is "' + test2 + '" not "teststringoverride".');
    }

    var test3 = self.engine.getConfig('test-module.numtest');
    if (test3 !== 1337) {
        throw new Error('Configuration test failed, configuration "numtest" is "' + test3 + '" not 1337.');
    }

    self.checkModules();

    var jsapiClient = self.engine.getSubmodule('kne-jsapi').getClient();
    jsapiClient.Services.User.list({query: {size: 1}}).then(function(res) {
        defer.resolve();
    }, function(err) {
        defer.reject(err);
    });
};

TestService.prototype.postInit = function(defer) {
    var self = this;

    self.log.debug('TestService post init check.');

    self.checkModules();

    self.checkRoutes().then(function() {
        self.checkModels().then(function() {
            defer.resolve();
        }, function(err) {
            defer.reject(err);
        });
    }, function(err) {
        defer.reject(err);
    });
};

TestService.prototype.checkRoutes = function() {
    var self = this,
        defer = Q.defer();

    self.log.debug('Routes check.');
    var req = http.get({
        host: '127.0.0.1',
        port: 8000,
        path: '/api/test'
    }, function(response) {
        var buffer = '';
        response.on('data', function(data) {
            buffer += data;
        });
        response.on('end', function() {
            try {
                var json = JSON.parse(buffer);
                if (json.success) {
                    defer.resolve();
                } else {
                    defer.reject(new Error('Invalid test response for route check.'));
                }
            } catch (err) {
                self.log.error('Error parsing test JSON:', buffer);
                defer.reject(err);
            }
        });
    });

    req.on('error', function(err) {
        defer.reject(err);
    });

    req.on('timeout', function() {
        req.abort();
    });

    return defer.promise;
};

TestService.prototype.checkModels = function() {
    var self = this,
        defer = Q.defer();

    try {
        self.log.debug('Models check');
        self.engine.getSubmodule('kne-mongo').getModel('Test');
        defer.resolve();
    } catch(err) {
        defer.reject(err);
    }

    return defer.promise;
};

TestService.prototype.checkModules = function() {
    var self = this;

    self.log.debug('Checking loaded submodules...');
    _.forEach(module.exports.submodules, function(name) {
        self.engine.getSubmodule(name);
        self.log.debug('Submodule loaded:', name);
    });
    self.log.debug('Submodules loaded.');
};

module.exports = TestService;
module.exports.initMaster = function(defer, engine, log) {
    log.debug('TestService master init check.');
    engine.getConfig('kne.workers');
    engine.getConfig('kne.logLevel');

    //The kneQueue submodule should be initialized on the master process.
    var kneQueue = engine.getSubmodule('kne-queue');
    kneQueue.getQueue();

    defer.resolve();
};
module.exports.configs = {
    test1: 'teststring',
    test2: 'teststring2',
    numtest: 1337
};
module.exports.submodules = [
    'kne-express',
    'kne-jsapi',
    'kne-mongo',
    'kne-redis',
    'kne-queue',
    'kne-websocket',
    'kne-mysql'
];
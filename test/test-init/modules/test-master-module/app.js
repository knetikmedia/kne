/**
 * Created by brandon on 3/21/16.
 */
/**
 * Created by brandon on 1/7/16.
 */

var _ = require('lodash'),
    Q = require('q');

function TestService() {
    var self = this;
}

TestService.prototype.init = function(defer) {
    var self = this;

    var test1 = self.engine.getConfig('test-master-module.test1');
    if (test1 !== 'teststring') {
        throw new Error('Configuration test failed, configuration "test1" is "' + test1 + '" not "teststring".');
    }

    var test2 = self.engine.getConfig('test-master-module.test2');
    if (test2 !== 'teststringoverride') {
        throw new Error('Configuration test failed, configuration "test2" is "' + test2 + '" not "teststringoverride".');
    }

    var test3 = self.engine.getConfig('test-master-module.numtest');
    if (test3 !== 1337) {
        throw new Error('Configuration test failed, configuration "numtest" is "' + test3 + '" not 1337.');
    }

    self.engine.getSubmodule('kne-queue');
    defer.resolve();
};

TestService.prototype.postInit = function(defer) {
    var self = this;

    self.log.debug('TestMasterService post init check.');

    self.checkModules();
    defer.resolve();
};

TestService.prototype.checkModules = function() {
    var self = this;

    self.log.debug('Checking loaded submodules...');
    _.forEach(module.exports.submodules, function(name) {
        self.engine.getSubmodule(name);
        self.log.debug('Submodule loaded:', name);
    });
    self.log.debug('Submodules loaded.');
};

module.exports = TestService;
module.exports.initMaster = true;
module.exports.configs = {
    test1: 'teststring',
    test2: 'teststring2',
    numtest: 1337
};
module.exports.submodules = [
    'kne-express',
    'kne-jsapi',
    'kne-mongo',
    'kne-redis',
    'kne-queue',
    'kne-websocket',
    'kne-mysql'
];
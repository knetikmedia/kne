module.exports = {
    'kne': {
        'workers': 4,
        'logLevel': 'info'
    },
    'kne-express': {
        'port': 8000
    },
    'kne-jsapi': {
        'url': 'http://jsapi2-integration.knetik.com:8080',
        'clientId': 'knetik',
        'clientSecret': 'vrgr98k73dK1*N!',
        'grantPath': '/oauth/token',
        'enableDebugging': false
    },
    'kne-mongo': {
        'url': 'mongodb://localhost/kne-default'
    },
    'kne-redis': {
        'url': 'redis://127.0.0.1:6379'
    },
    'kne-queue': {
        'redisUrl': 'redis://127.0.0.1:6379',
        'removeJobsOnComplete': false,
        'uiPort': '3000'
    },
    'kne-websocket': {
        'port': 14001
    },
    'kne-mysql': {
        'host': '127.0.0.1',
        'port': 3306,
        'user': 'root',
        'password': 'mysql',
        'connectionLimit': 2,
        'options': {}
    },
    'test-master-module': {
        'test1': 'teststring',
        'test2': 'teststringoverride',
        'numtest': 1337
    },
    'test-module': {
        'test1': 'teststring',
        'test2': 'teststringoverride',
        'numtest': 1337
    }
};
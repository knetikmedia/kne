global.KNE_BASE_PATH = __dirname + '/test-init';

var assert = require('assert'),
    should = require('should'),
    engine = require('../app'),
    cluster = require('cluster');

describe('Engine', function() {
    it('should be initialized', function(done) {
        this.timeout(20000);
        engine.init([
            'test-module',
            'test-master-module'
        ]).then(function(defer) {
            if (cluster.isMaster) {
                describe('Master', function() {
                    it('should be initialized', function(done) {
                        this.timeout(20000);
                        done();
                    });

                    describe('#getModule()', function() {
                        it('should return the specified master module', function() {
                            engine.getModule('test-master-module').should.be.instanceOf(Object);
                        });
                        it('should throw an error when attempting to get an undefined master module', function() {
                            assert.throws(function() {
                                engine.getModule('fake-module');
                            });
                        });
                    });

                    describe('#getSubmodule()', function() {
                        it('should return the specified master submodule', function() {
                            engine.getSubmodule('kne-queue').should.be.instanceOf(Object);
                        });
                        it('should throw an error when attempting to get an undefined master submodule', function() {
                            assert.throws(function() {
                                engine.getSubmodule('fake-submodule');
                            });
                        });
                    });

                    describe('#getConfig()', function() {
                        it('should return the specified configuration', function() {
                            engine.getConfig('test-module.test1').should.equal('teststring');
                        });
                        it('should throw an error when attempting to get an undefined configuration', function() {
                            assert.throws(function() {
                                engine.getConfig('should.not.exist.fake');
                            });
                        });
                    });
                });

                done();
            } else {
                describe('Worker', function() {
                    describe('#getModule()', function() {
                        it('should return the specified module', function() {
                            engine.getModule('test-module').should.be.instanceOf(Object);
                        });
                        it('should throw an error when attempting to get an undefined module', function() {
                            assert.throws(function() {
                                engine.getModule('fake-module');
                            });
                        });
                    });

                    describe('#getSubmodule()', function() {
                        it('should return the specified submodule', function() {
                            engine.getSubmodule('kne-mongo').should.be.instanceOf(Object);
                        });
                        it('should throw an error when attempting to get an undefined submodule', function() {
                            assert.throws(function() {
                                engine.getSubmodule('fake-submodule');
                            });
                        });
                    });

                    it('should be initialized', function(done) {
                        this.timeout(20000);
                        done();
                    });
                });

                var origExit = process.exit;
                setTimeout(function() {
                    origExit(0);
                }, 5000);
                process.exit = function() {}; //Stop Mocha from exiting when test completed.

                done();
                defer.resolve();
            }
        }, function(err) {
            done(err);
        });
    });
});



/**
 * Created by brandon on 1/7/16.
 */

'use strict';
/* jshint strict: true */
/* jshint node: true */

var configPath = null,
    loadedConfigs = null,
    creatingConfigs = false,
    configs = {},
    fs = require('fs'),
    cluster = require('cluster'),
    _ = require('lodash'),
    bunyan = require('bunyan'),
    Q = require('q'),
    EventEmitter = require('events').EventEmitter,
    log = null;

//Default configurations for the engine.
var defaultEngineConfigs = {
    'workers': 4,
    'logLevel': 'info'
};

//Base modules located in the engine.
var submodulesAvailable = {
    'kne-express': require('./submodules/kneExpress'),
    'kne-mongo': require('./submodules/kneMongo'),
    'kne-mysql': require('./submodules/kneMySql'),
    'kne-jsapi': require('./submodules/kneJsapi'),
    'kne-redis': require('./submodules/kneRedis'),
    'kne-websocket': require('./submodules/kneWebsocket'),
    'kne-queue': require('./submodules/kneQueue')
};

function Engine() {
    var self = this,
        submodules = {},
        modules = {};

    self.hasInitialized = false;
    Q.longStackSupport = true;

    if (!global.KNE_LOG_PATH && global.KNE_BASE_PATH) {
        global.KNE_LOG_PATH = global.KNE_BASE_PATH + '/logs/';
    }

    if (!global.KNE_CONFIG_PATH && global.KNE_BASE_PATH) {
        global.KNE_CONFIG_PATH = global.KNE_BASE_PATH + '/configs/';
    }

    //Initializes workers and all KNE modules.
    var moduleNames = null;
    self.init = function(modulesToLoad) {
        var initDefer = Q.defer();
        moduleNames = modulesToLoad;

        process.on('uncaughtException', function (err) {
            if (log) {
                log.error('Uncaught Exception:', err.stack ? err.stack : err);
            }
            console.error('Uncaught Exception:', err.stack ? err.stack : err);
            process.exit(1);
        });

        process.on('unhandledRejection', function (err) {
            if (log) {
                log.error("Unhandled Rejection:", err, err.stack);
            }
            console.error("Unhandled Rejection:", err, err.stack);
            process.exit(1);
        });

        if (!global.KNE_BASE_PATH) {
            console.error('The path configuration KNE_BASE_PATH must be defined in global scope.');
            process.exit(1);
        }
        configPath = global.KNE_CONFIG_PATH + 'config.js';

        if (cluster.isMaster) {
            process.title = 'kne-master';
            console.time('Engine Init Time');
            console.log('Initializing engine... NODE_ENV: ' + process.env.NODE_ENV);

            if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'development' && process.env.NODE_ENV !== 'testing') {
                console.error('Environment variable NODE_ENV must be set to either "production", "development", or "testing".');
                process.exit(1);
            }

            if (process.env.NODE_ENV == 'production') {
                createFolder(global.KNE_LOG_PATH);
            }
            createFolder(global.KNE_CONFIG_PATH);

            checkConfigs();
            createLogger();
            log.info('Initializing engine... NODE_ENV: ' + process.env.NODE_ENV);
            if (!checkModules(moduleNames)) {
                initDefer.reject('Invalid config.');
                return initDefer.promise;
            }

            initModulesMaster().then(function() {
                initWorkers(initDefer);
            }, function(err) {
                initDefer.reject(err);
                console.error('Failed to init master modules:', err);
                process.exit(1);
            });

            return initDefer.promise;
        } else {
            process.title = 'kne-worker' + cluster.worker.id;
            self.sendWorkerMessage = function (type, data) {
                process.send({id: cluster.worker.id, type: type, data: data});
            };

            checkConfigs();
            createLogger();
            log.info('W-' + cluster.worker.id + ' initializing...');
            initModules(initDefer);
        }

        return initDefer.promise;
    };

    self.getModule = function(name) {
        var m = modules[name];
        if (!m) {
            throw new Error('Module "' + name + '" does not exist or has not been initialized.');
        }
        return m;
    };

    self.getSubmodule = function(name) {
        var m = submodules[name];
        if (!m) {
            throw new Error('Submodule "' + name + '" does not exist or has not been initialized.');
        }
        return m;
    };

    self.getConfig = function(name) {
        var value = _.get(configs, name);
        if (value === undefined) {
            throw new Error('Config "' + name + '" was not found and does not have a default set.');
        }
        return value;
    };

    self.getModulePaths = function() {
        var obj = {};
        _.forEach(moduleNames, function(name) {
            try {
                var path =  global.KNE_BASE_PATH + '/modules/' + name + '/app.js';
                fs.accessSync(path, fs.F_OK);
                obj[name] = path;
            } catch (e) {
                obj[name] = name;
            }
        });
        return obj;
    };

    function createLogger() {
        if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'testing') {
            var useBunyan = !process.env.KNE_DISABLE_BUNYAN;
            if (useBunyan) {
                var spawn = require('child_process').spawn;
                self.bunyanCLI = spawn('bunyan', ['--color'], {stdio: ['pipe', process.stdout]});
                self.bunyanCLI.on('error', function (err) {
                    console.error('Error starting Bunyan(Is it installed?):', err.stack ? err.stack : err);
                    process.exit(1);
                });
            } else {
                var Stream = require('stream');
                var stream = Stream.PassThrough();
                stream.on('data', function(data) {
                    data = data.toString('utf8');
                    try {
                        data = JSON.parse(data);
                    } catch (e) {
                        data = {
                            pid: process.pid,
                            level: 30,
                            msg: data,
                            time: new Date()
                        };
                    }

                    var level = bunyan.nameFromLevel[data.level];
                    if (!level) {
                        level = 'INFO';
                    }

                    if (level === 'error') {
                        console.error('[' + data.time + ']', level.toUpperCase() + ':', data.msg);
                    } else {
                        console.log('[' + data.time + ']', level.toUpperCase() + ':', data.msg);
                    }
                });

                self.bunyanCLI = {
                    stdin: stream
                };
            }

            log = bunyan.createLogger({
                name: 'engine',
                streams: [{stream: self.bunyanCLI.stdin}],
                level: self.getConfig('kne.logLevel')
            });
        } else {
            log = bunyan.createLogger({
                name: 'engine',
                streams: [{path: global.KNE_LOG_PATH + 'engine.log'}],
                level: self.getConfig('kne.logLevel')
            });
        }
    }

    function checkConfigs() {
        try {
            loadedConfigs = require(configPath);
            if (loadedConfigs.kne) {
                configs = {
                    kne: loadedConfigs.kne
                };
            }
        } catch (e) {
            console.error('Failed to load configuration file at "' + configPath + '".');
            if (e instanceof Error && e.code === 'MODULE_NOT_FOUND') {
                creatingConfigs = true;
                configs = {
                    kne: defaultEngineConfigs
                };
            } else {
                console.error(e.stack ? e.stack : e);
                process.exit(1);
            }
        }
    }

    function checkModules() {
        //Check module dependencies.
        var submoduleNames = {};
        _.forEach(moduleNames, function(name) {
            try {
                var moduleRequire = require(self.getModulePaths()[name]);
                _.forEach(moduleRequire.submodules, function (subname) {
                    var submoduleRequire = submodulesAvailable[subname];
                    if (submoduleRequire) {
                        submoduleNames[subname] = submoduleRequire;
                    } else {
                        console.error('Invalid submodule "' + subname + '" required by module "' + name + '".');
                        process.exit(1);
                    }
                });
            } catch (err) {
                console.error('Failed to load module "' + name + '" due to an error:', err.stack ? err.stack : err);
                process.exit(1);
            }
        });

        if (!checkModuleConfigs(submoduleNames)) {
            return null;
        }

        return submoduleNames;
    }

    var configurationsUpdated = false,
        configurationsAdded = false,
        configurationError = false;
    function checkModuleConfigs(submoduleNames) {
        if (configurationsUpdated) {
            return true;
        }

        //Check engine configs.
        compareConfigs('kne', defaultEngineConfigs, loadedConfigs);

        //Check submodule configs.
        _.forEach(submoduleNames, function(submoduleRequire, subname) {
            compareConfigs(subname, submoduleRequire.configs, loadedConfigs);
        });

        //Check module configs.
        _.forEach(moduleNames, function(name) {
            var moduleRequire = require(self.getModulePaths()[name]);
            compareConfigs(name, moduleRequire.configs, loadedConfigs);
        });

        //Update configuration if there is an error.
        if (configurationError) {
            createConfigFile();
            process.exit(1);
            return false;
        }

        //Create config file if it does not exist.
        if (creatingConfigs) {
            createConfigFile();
            process.exit(0);
            return false;
        } else {
            if (cluster.isMaster && configurationsAdded) {
                createConfigFile();
                configurationsUpdated = true;
            }
        }

        function compareConfigs(name, defined, loaded) {
            if (!configs[name]) {
                configs[name] = {};
            }
            if (loaded) {
                loaded = loaded[name];
            }

            _.forEach(defined, function(value, key) {
                if (loaded !== undefined && loaded !== null && loaded[key] !== undefined) {
                    configs[name][key] = loaded[key];
                } else {
                    if (value === undefined) {
                        configs[name][key] = 'undefined';
                        if (!creatingConfigs) {
                            console.error('Configuration key "' + key + '" in module "' + name + '" is not defined in "configs/config.js" and a default is not set.');
                            configurationError = true;
                        }
                    } else {
                        console.log('New configuration added: ' + name + '.' + key);
                        configurationsAdded = true;
                        configs[name][key] = value;
                    }
                }
            });
        }

        return true;
    }

    function createConfigFile() {
        console.log('Creating/updating configuration file in "' + configPath + '".');

        _.forEach(configs, function(value, key) {
            if (typeof value === 'object' && Object.keys(value).length <= 0) {
                delete configs[key];
            }
        });

        var configString = 'module.exports = ';
        configString += JSON.stringify(configs, null, 4) + ';';
        configString = configString.replace(/\"/g, '\'');
        configString = configString.replace(/'undefined'/g, 'undefined');

        try {
            fs.writeFileSync(configPath, configString);
        } catch (err) {
            console.error('Failed to create/update configuration file:', err.stack ? err.stack : err);
            process.exit(1);
        }
    }

    function createFolder(path) {
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }
    }

    function initModulesMaster() {
        var submoduleNames = checkModules(),
            modulesLeft = _.clone(moduleNames),
            deferred = Q.defer();
        log.info('Master process initializing...');
        initSubmoduleMaster();

        function initSubmoduleMaster() {
            if (Object.keys(submoduleNames).length <= 0) {
                initModuleMaster();
                return;
            }

            var name = Object.keys(submoduleNames).shift();
            try {
                var moduleRequire = submoduleNames[name];
                delete submoduleNames[name];

                if (moduleRequire.initMaster) {
                    log.info('Master Init Submodule: ' + name);

                    var defer = Q.defer();
                    if (typeof moduleRequire.initMaster === 'function') {
                        moduleRequire.initMaster(defer, self, log);
                    } else {
                        var moduleInstance = new moduleRequire();
                        if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'testing') {
                            moduleInstance.log = bunyan.createLogger({
                                name: 'engine-' + name,
                                streams: [{stream: self.bunyanCLI.stdin}],
                                level: self.getConfig('kne.logLevel')
                            });
                        } else {
                            moduleInstance.log = bunyan.createLogger({
                                name: 'engine-' + name,
                                streams: [{path: global.KNE_LOG_PATH + 'engine.log'}],
                                level: self.getConfig('kne.logLevel')
                            });
                        }
                        moduleInstance.engine = self;
                        moduleInstance.init(defer, true);
                        submodules[name] = moduleInstance;
                    }
                    defer.promise.then(function() {
                        initSubmoduleMaster();
                    }, function(err) {
                        console.error('Submodule "' + name + '" master init error:', err.stack ? err.stack : err);
                        process.exit(1);
                    });
                } else {
                    initSubmoduleMaster();
                }
            } catch (err) {
                console.error('Submodule "' + name + '" master init error:', err.stack ? err.stack : err);
                process.exit(1);
            }
        }

        function initModuleMaster() {
            if (modulesLeft.length <= 0) {
                log.info('Master process initialized.');
                deferred.resolve();
                return;
            }

            var name = modulesLeft[0];
            try {
                var moduleRequire = require(self.getModulePaths()[name]);
                modulesLeft.splice(0, 1);

                if (moduleRequire.initMaster) {
                    log.info('Master Init Module: ' + name);

                    var defer = Q.defer();
                    if (typeof moduleRequire.initMaster === 'function') {
                        moduleRequire.initMaster(defer, self, log);
                    } else {
                        var moduleInstance = new moduleRequire();
                        if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'testing') {
                            moduleInstance.log = bunyan.createLogger({
                                name: 'engine-' + name,
                                streams: [{stream: self.bunyanCLI.stdin}],
                                level: self.getConfig('kne.logLevel')
                            });
                        } else {
                            moduleInstance.log = bunyan.createLogger({
                                name: 'engine-' + name,
                                streams: [{path: global.KNE_LOG_PATH + 'engine.log'}],
                                level: self.getConfig('kne.logLevel')
                            });
                        }
                        moduleInstance.engine = self;
                        moduleInstance.init(defer, true);
                        modules[name] = moduleInstance;
                    }
                    defer.promise.then(function() {
                        initModuleMaster();
                    }, function(err) {
                        console.error('Module "' + name + '" master init error:', err.stack ? err.stack : err);
                        process.exit(1);
                    });
                } else {
                    initModuleMaster();
                }
            } catch (err) {
                console.error('Module "' + name + '" master init error:', err.stack ? err.stack : err);
                process.exit(1);
            }
        }

        return deferred.promise;
    }

    function initModules(initDefer) {
        var submoduleNames = checkModules(),
            modulesLeft = _.clone(moduleNames);
        initSubmodule();

        function initSubmodule() {
            if (Object.keys(submoduleNames).length <= 0) {
                initModule();
                return;
            }

            var name = Object.keys(submoduleNames).shift(),
                defer = Q.defer();
            log.info('Init Submodule: ' + name);

            try {
                var moduleRequire = submoduleNames[name];
                if (typeof moduleRequire != 'function') {
                    console.warn('Submodule "' + name + '" does not export a function and can not be instantiated.');
                    process.exit(1);
                }

                var moduleInstance = new moduleRequire();
                delete submoduleNames[name];

                if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'testing') {
                    moduleInstance.log = bunyan.createLogger({
                        name: 'engine-' + name,
                        streams: [{stream: self.bunyanCLI.stdin}],
                        level: self.getConfig('kne.logLevel')
                    });
                } else {
                    moduleInstance.log = bunyan.createLogger({
                        name: 'engine-' + name,
                        streams: [{path: global.KNE_LOG_PATH + 'engine.log'}],
                        level: self.getConfig('kne.logLevel')
                    });
                }
                moduleInstance.engine = self;
                moduleInstance.init(defer);
                submodules[name] = moduleInstance;

                defer.promise.then(function() {
                    try {
                        initSubmodule();
                    } catch (err) {
                        console.error('Submodule init error:', err.stack ? err.stack : err);
                        process.exit(1);
                    }
                }, function(err) {
                    console.warn('Failed to init submodule "' + name + '":', err.stack ? err.stack : err);
                    cluster.worker.kill();
                });
            } catch (err) {
                console.error('Submodule "' + name + '" init error:', err.stack ? err.stack : err);
                process.exit(1);
            }
        }

        function initModule() {
            if (modulesLeft.length <= 0) {
                postInitModules().then(function() {
                    log.info('W-' + cluster.worker.id + ' initialized.');
                    if (process.env.NODE_ENV === 'testing') {
                        var testDefer = Q.defer();
                        initDefer.resolve(testDefer);
                        testDefer.promise.then(function() {
                            self.sendWorkerMessage('init', true);
                        }, function(err) {
                            throw err;
                        });
                    } else {
                        self.sendWorkerMessage('init', true);
                        initDefer.resolve();
                    }
                }, function(err) {
                    initDefer.reject(err);
                });

                return;
            }

            var name = modulesLeft[0],
                defer = Q.defer();
            log.info("Init Module: " + name);

            try {
                var moduleRequire = require(self.getModulePaths()[name]),
                    moduleInstance = new moduleRequire();
                modulesLeft.splice(0, 1);

                if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'testing') {
                    moduleInstance.log = bunyan.createLogger({
                        name: name,
                        streams: [{stream: self.bunyanCLI.stdin}],
                        level: self.getConfig('kne.logLevel')
                    });
                } else {
                    moduleInstance.log = bunyan.createLogger({
                        name: name,
                        streams: [{path: global.KNE_LOG_PATH + name + '.log'}],
                        level: self.getConfig('kne.logLevel')
                    });
                }
                moduleInstance.engine = self;
                moduleInstance.init(defer);
                modules[name] = moduleInstance;

                defer.promise.then(function() {
                    initModule();
                }, function(err) {
                    console.warn('Failed to init module "' + name + '":', err.stack ? err.stack : err);
                    cluster.worker.kill();
                });
            } catch (err) {
                console.error('Module "' + name + '" init error:', err.stack ? err.stack : err);
                process.exit(1);
            }
        }
    }

    function postInitModules() {
        var defer = Q.defer(),
            submodulesLeft = checkModules(),
            modulesLeft = _.clone(moduleNames);

        postInitSubmodule();
        function postInitSubmodule() {
            if (Object.keys(submodulesLeft).length > 0) {
                var moduleDefer = Q.defer(),
                    name = Object.keys(submodulesLeft)[0],
                    module = submodules[name];
                delete submodulesLeft[name];

                if (typeof module.postInit === 'function') {
                    try {
                        log.info("Post Init Submodule: " + name);
                        module.postInit(moduleDefer);
                        moduleDefer.promise.then(function() {
                            postInitSubmodule();
                        }, function(err) {
                            console.error('Submodule "' + name + '" post init error:', err.stack ? err.stack : err);
                            process.exit(1);
                        });
                    } catch (err) {
                        console.error('Submodule "' + name + '" post init error:', err.stack ? err.stack : err);
                        process.exit(1);
                    }
                } else {
                    postInitSubmodule();
                }
            } else {
                postInitModule();
            }
        }

        function postInitModule() {
            if (Object.keys(modulesLeft).length > 0) {
                var moduleDefer = Q.defer(),
                    name = modulesLeft[0],
                    module = modules[name];
                modulesLeft.splice(0, 1);
                if (typeof module.postInit === 'function') {
                    try {
                        log.info("Post Init Module: " + name);
                        module.postInit(moduleDefer);
                        moduleDefer.promise.then(function() {
                            postInitModule();
                        }, function(err) {
                            console.error('Module "' + name + '" post init error:', err.stack ? err.stack : err);
                            process.exit(1);
                        });
                    } catch (err) {
                        console.error('Module "' + name + '" post init error:', err.stack ? err.stack : err);
                        process.exit(1);
                    }
                } else {
                    postInitModule();
                }
            } else {
                defer.resolve();
            }
        }

        return defer.promise;
    }

    function initWorkers(initDefer) {
        self.workerHandler = new EventEmitter();

        cluster.on('message', function(data) {
            self.workerHandler.emit(data.type, data.id, data.data);
        });

        cluster.on('exit', function (worker, code) {
            console.warn('WORKER DIED W-' +  worker.id + ':', code);
            log.error('WORKER DIED W-' +  worker.id + ':', code);
            if (self.hasInitialized) {
                cluster.fork();
            } else {
                process.exit(1);
            }
        });

        var workerSize = self.getConfig('kne.workers');
        console.log('Forking and initializing ' + workerSize + ' workers.');
        cluster.fork();

        self.workerHandler.on('init', function(id, data) {
            if (!self.hasInitialized) {
                if (Object.keys(cluster.workers).length < workerSize) {
                    cluster.fork();
                } else {
                    initDefer.resolve();
                    self.hasInitialized = true;
                    console.timeEnd('Engine Init Time');
                    console.log('Engine initialized.');
                }
            }
        });
    }
}

module.exports = new Engine();